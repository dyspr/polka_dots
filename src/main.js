var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var arraySize = 3
var array = create2DArray(arraySize,  arraySize, 0, false)
var initSize = 0.2

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < arraySize + 1; i++) {
    for (var j = 0; j < arraySize + 1; j++) {
      push()
      translate(Math.floor(windowWidth * 0.5 + (i - Math.floor(arraySize * 0.5) - 0.5) * boardSize * initSize), Math.floor(windowHeight * 0.5 + (j - Math.floor(arraySize * 0.5) - 0.5) * boardSize * initSize))
      if ((i + j) % 2 === 0) {
        fill(255)
      } else {
        fill(0)
      }
      ellipse(0, 0, (boardSize * initSize))
      pop()
    }
  }

  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      push()
      translate(Math.floor(windowWidth * 0.5 + (i - Math.floor(arraySize * 0.5)) * boardSize * initSize), Math.floor(windowHeight * 0.5 + (j - Math.floor(arraySize * 0.5)) * boardSize * initSize))
      truchetTile(Math.floor(boardSize * initSize + 3), array[i][j][0], (i + j) % 2)
      pop()
    }
  }

  array[Math.floor(Math.random() * arraySize)][Math.floor(Math.random() * arraySize)] = [Math.floor(Math.random() * 2), Math.floor(Math.random() * 2)]
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function truchetTile(size, type, rotation) {
  if (type === 0) {
    if (rotation === 0) {
      fill(255)
      noStroke()
      rect(0, 0, size, size)
      push()
      translate(size * 0.5, size * 0.5)
      fill(0)
      arc(0, 0, size, size, -Math.PI, -Math.PI * 0.5)
      pop()
      push()
      translate(-size * 0.5, -size * 0.5)
      fill(0)
      arc(0, 0, size, size, 0, Math.PI * 0.5)
      pop()
    } else {
      fill(255)
      noStroke()
      rect(0, 0, size, size)
      push()
      translate(size * 0.5, -size * 0.5)
      fill(0)
      arc(0, 0, size, size, Math.PI * 0.5, Math.PI)
      pop()
      push()
      translate(-size * 0.5, size * 0.5)
      fill(0)
      arc(0, 0, size, size, -Math.PI * 0.5, 0)
      pop()
    }
  } else {
    if (rotation === 0) {
      fill(0)
      noStroke()
      rect(0, 0, size, size)
      push()
      translate(size * 0.5, size * 0.5)
      fill(255)
      arc(0, 0, size, size, -Math.PI, -Math.PI * 0.5)
      pop()
      push()
      translate(-size * 0.5, -size * 0.5)
      fill(255)
      arc(0, 0, size, size, 0, Math.PI * 0.5)
      pop()
    } else {
      fill(0)
      noStroke()
      rect(0, 0, size, size)
      push()
      translate(size * 0.5, -size * 0.5)
      fill(255)
      arc(0, 0, size, size, Math.PI * 0.5, Math.PI)
      pop()
      push()
      translate(-size * 0.5, size * 0.5)
      fill(255)
      arc(0, 0, size, size, -Math.PI * 0.5, 0)
      pop()
    }
  }
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = Math.floor(Math.random() * steps)
      } else {
        columns[j] = [Math.floor(Math.random() * 2), Math.floor(Math.random() * 2)]
      }
    }
    array[i] = columns
  }
  return array
}
